def dictwithout(dict: dict, key: str) -> dict:
    '''

    :param dict: dictionary from the key shall be removed by using .pop()
    :param key: key which shall be removed
    :return: the updated dictionary with the key removed
    '''
    dict = dict
    dict.pop(key)
    return dict

########################################################################################################################

def getStudentsDict(file: str, idKey: str) -> dict:
    '''
    Dependecies: Pandas (is import in function)
    :param file: location for file with student info, must be a csv(encoding =UTF-16)
    :param idKey: Key which identifies the Student e.g column titled MatNr
    :return: dict with idKey in the form of {idKey: {other infor}
    '''
    import pandas as pd                                                                                                 #for nicer management and handling of data/csv-files
    studentsTable = pd.read_csv(file, encoding='UTF-16')                                                                #reads in the csv file with student info
    listDict = studentsTable.to_dict('records')                                                                         #list with dict for every student
    studentsDict = {}                                                                                                   #init studentsDict

    for dict in listDict:
        studentsDict.update({str(dict[idKey]): dictwithout(dict, idKey)})                                                #add key to studentsDict --> remove key from dict in list dict(will be removed globally in original dict) --> add remaining dict info to studentsDict

    return studentsDict


import os                                                                                                               #for file management
import smtplib                                                                                                          #for connection to SMTP Server to send mails
from email.message import EmailMessage                                                                                  #For nicer Mail text and Attachments
from getStudents import *

#Functions##############################################################################################################

# def dictwithout(dict: dict, key: str) -> dict:
#     '''
#
#     :param dict: dictionary from the key shall be removed by using .pop()
#     :param key: key which shall be removed
#     :return: the updated dictionary with the key removed
#     '''
#     dict = dict
#     dict.pop(key)
#     return dict
#
# ########################################################################################################################
#
# def getStudentsDict(file: str, idKey: str) -> dict:
#     '''
#     Dependecies: Pandas (is import in function)
#     :param file: location for file with student info, must be a csv(encoding =UTF-16)
#     :param idKey: Key which identifies the Student e.g column titled MatNr
#     :return: dict with idKey in the form of {idKey: {other infor}
#     '''
#     import pandas as pd                                                                                                 #for nicer management and handling of data/csv-files
#     studentsTable = pd.read_csv(file, encoding='UTF-16')                                                                #reads in the csv file with student info
#     listDict = studentsTable.to_dict('records')                                                                         #list with dict for every student
#     studentsDict = {}                                                                                                   #init studentsDict
#
#     for dict in listDict:
#         studentsDict.update({str(dict[idKey]): dictwithout(dict, idKey)})                                                #add key to studentsDict --> remove key from dict in list dict(will be removed globally in original dict) --> add remaining dict info to studentsDict
#
#     return studentsDict

########################################################################################################################

studentTablePath = input('Name or Path of .csv (UTF-16) file with student info: ')                                      #e.g students.csv with student info: (MatNr, Name, Mail)
studenIdKey = input('Column name for identifier in file name: ')                                                        #to find associated file to student (MUST BE UNIQUE FOR EACH STUDENT!) e.g MatNr
mailKey = input('Column name for Mail-Adress: ')                                                                        #To find Mail Address, e.g. Mail

filesFolder = input('Relative path to folder with files to send: ')                                                     #Folder in which the pdf files are found

seperator = input('Seperator for parts of file name: ')                                                                 #To know where to split the file name, e.g. A_B_C with seperator = '_' --> [A, B, C]

posStudentId = int(input('Number of the part of the file name where the student id is found: '))                        #In which location the identifier is found, e.g if Identifier is C, then posStudentId = 2 or -1

studentsDict = getStudentsDict(studentTablePath, studenIdKey)

print(studentsDict)
files = os.listdir(filesFolder)                                                                                         #look what files are in the filesFolder

studentIds = []                                                                                                         #List of studentIds found in fileFolder
for file in files:                                                                                                      #find the StudentId for each file
    try:
        id = file.split('.')[0].split(seperator)[posStudentId]
        studentsDict[id].update({'file': file})
        print(studentsDict[id]['file'] + ' was assigned to Student with identifier ' + str(id) + ' mit Mail Addresse ' + studentsDict[id][mailKey])

        studentIds.append(id)
    except KeyError:                                                                                                    #Error if Id couldnt be found as Key in StudentsDict
        print("KeyError: StudentId: " + str(id) +" not in list, removing id")
        #studentIds.remove(id)

########################################################################################################################
print('Current Assignments: ' + str(studentsDict))
if input('To abord and exit type \'exit()\', to send the Mails press Enter' ) == 'exit()':                              #Exit if something is wrong
    exit()
#Login-EMail-Account####################################################################################################
user = input('User-Name: ')
pwd =  input('Passowrd: ')

userMail = input('Mail-Adress: ')

#Choose-SMTP-Server#####################################################################################################

SMTPAddress = input('Enter \'STMP-Address:Port\': ')                                                                    #For Uni-Bremen: smtp.uni-bremen.de:587

server = smtplib.SMTP(SMTPAddress)

########################################################################################################################

subject = input('Enter Mail Subject: ')                                                                                 #Mail subject
message = input('Enter Mail message: ')                                                                                 #Mail message/text

server.starttls()                                                                                                       #connect to sever (ttls is for Encryption

server.login(user, pwd)                                                                                                 #login to server

for id in studentIds:                                                                                                   #Write and send mail to each student
    mailTo  = studentsDict[id][mailKey]

    msg = EmailMessage()
    msg['Subject'] = subject
    msg['From'] = userMail
    msg['To'] = mailTo
    msg.set_content(message)
    with open(filesFolder + '/' + studentsDict[id]['file'], 'rb') as attachment_file:                                   #Attach file for specific student
        attachment = attachment_file.read()
        msg.add_attachment(attachment, maintype='application', subtype='pdf', filename=studentsDict[id]['file'])

    server.send_message(msg)
server.quit()                                                                                                           #Disconnect from SMTP server

########################################################################################################################
